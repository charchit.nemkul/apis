from gettext import find
from fastapi import APIRouter,HTTPException, Depends 
from fastapi.security import OAuth2PasswordBearer,OAuth2PasswordRequestForm
from models.user import User, UserOut, UserIn
from db import conn
from schemas.user import userEntity, usersEntity
from bson import ObjectId
from passlib.context import CryptContext




user = APIRouter()
user_db = conn.test.user
oauth2 = OAuth2PasswordBearer(tokenUrl = "token") #returns a token to be stored in frontend when username/password is entered
# why is token the username?

def fake_hashed_password(password:str):
    return "hashed" + password 

def get_user(db,username:str): 
    print(db)
    print(username)
    if username == db['username']:
        user_dict = db
        return UserIn(**user_dict)


def decode_token(token):
    # this needs to provide security 
    user = get_user(conn.test.user.find_one(),token)
    return user 


async def get_current_users(token: str = Depends(oauth2)):
    print(token)
    user = decode_token(token)
    return user






@user.post("/token")
async def login(form_data: OAuth2PasswordRequestForm = Depends()):
    
    user_dict = user_db.find_one({"username":form_data.username}) # put id here 
    print(user_dict)
    user = UserIn(**user_dict)
    hashed_password = fake_hashed_password(form_data.password)
    if not hashed_password == user.hashed_password:
        raise HTTPException(status_code=400, detail="Incorrect username or password")

    return {"access_token": user.username, "token_type": "bearer"}


@user.get("/users/me")
async def find_all_users(current_user: User = Depends(get_current_users)):
    # print(conn.test.user.find())  #converts mongo obhject id to python dictionary 
    # print(usersEntity(conn.test.user.find()))
    # x = conn.test.user.find()
    # if x.count() == 0:
    #     raise raise_user_not_found()
    # return usersEntity(conn.test.user.find())
    print(current_user)
    return current_user



# @user.post("/",response_model=UserOut)
# async def create_user(user:User): # Pydantic model defined inside model.user
#     conn.test.user.insert_one(dict(user))    
#     return user
#     # return userEntity(conn.test.user.find()) # response models dont work when i return mongodb query 


@user.put("/{id}")
async def update_users(id,user:User):
    conn.test.user.find_one_and_update({"_id":ObjectId(id)},{"$set":dict(user)})

    return userEntity(conn.test.user.find_one({"_id":ObjectId(id)}))

@user.patch("/{id}")
async def update_selective(id):
    conn.test.user.update_one
    
@user.delete("/{id}")
async def delete_users(id):

    x = conn.test.user.find()
    if x.count() == 0: 
        raise raise_user_not_found()
    conn.test.user.delete_one({"_id":ObjectId(id)})
    return usersEntity(conn.test.user.find())


    

def raise_user_not_found(): # HTTP exception    
    return HTTPException(status_code = 404, detail = "No users detected",headers={"X-header_error":"Nothing to be seen here"})