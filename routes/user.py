import time 
from datetime import datetime, timedelta
from fastapi import APIRouter,HTTPException, Depends, status 
from fastapi.security import OAuth2PasswordBearer,OAuth2PasswordRequestForm
from models.user import User, UserOut, UserIn, Token, TokenData
from db import conn
from schemas.user import userEntity, usersEntity
from bson import ObjectId
from passlib.context import CryptContext
from jose import JWTError, jwt   
from typing import Union
from fastapi.responses import JSONResponse

SECRET = "fd2af3886a6b435ecf81eb37ac6f7b904d298f23fcefbb0c03ff0558770a4f40"
ALGORITHM = "HS256" 
ACCESS_TOKEN_EXPIRATION = 30 

user = APIRouter()
user_db = conn.test.user
oauth2 = OAuth2PasswordBearer(tokenUrl = "token") #returns a token to be stored in frontend when username/password is entered
# why is token the username?

pwd_context = CryptContext(schemes = ["bcrypt"], deprecated = "auto")

def get_password_hash(password):
    return pwd_context.hash(password)

def verify_password(plain_password,hashed_password):
    print(plain_password)
    print(hashed_password)
    return pwd_context.verify(plain_password,hashed_password)


def authenticate_user(db,username:str, password:str):
    # user = get_user(db,username)
    # print(user)
    # hashed_password = get_password_hash(user.hashed_password)    
    # conn.test.user.update_one({"username":username},{'$set':{"hashed_password":hashed_password}})   
    # # conn.close()
    # time.sleep(30)
    # # conn.start_session()
    user = get_user(db,username)
    if not user:
        return False    
    if not verify_password(password,user.hashed_password):
        return False 
    return user

def get_user(db,username:str): 
    print(db)
    print(username)
    if username == db['username']:
        user_dict = db
        return UserIn(**user_dict)


def decode_token(token):
    # this needs to provide security 
    user = get_user(conn.test.user.find_one(),token)
    return user 


async def get_current_users(token: str = Depends(oauth2)):
    # print(token)
    # user = decode_token(token)
    # return user
    credentail_exception = HTTPException(status_code = status.HTTP_401_UNAUTHORIZED,
                                        detail = "could not validate credentias",
                                        headers = {"WWW-Authenticate": "Bearer"})

    try:
        payload = jwt.decode(token,SECRET, algorithms= [ALGORITHM]) 
        username: str = payload.get("sub") 
        if username is None:
            raise credentail_exception
        token_data = TokenData(username = username)
    except JWTError:
        raise credentail_exception
    user = get_user(conn.test.user.find_one(),username = token_data.username)
    if user is None:
        raise credentail_exception
    return user




def generate_new_token(data:dict, expires_delta: Union[timedelta,None] = None):
    to_encode = data.copy()
    if expires_delta:
        expire = datetime.utcnow() + expires_delta
    else:
        expire = datetime.utcnow() + timedelta(minutes = 15)
    to_encode.update({"exp":expire})
    encoded_jwt =   jwt.encode(to_encode,SECRET, algorithm = ALGORITHM)
    return encoded_jwt


@user.post("/token",response_model = Token)
async def login_for_access_token(form_data: OAuth2PasswordRequestForm = Depends()):
    user = authenticate_user(conn.test.user.find_one(),form_data.username, form_data.password)
    if not user:
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail="Incorrect username or password",
            headers={"WWW-Authenticate": "Bearer"},
        )
    access_token_expires = timedelta(minutes=ACCESS_TOKEN_EXPIRATION)
    access_token = generate_new_token(
        data={"sub": user.username}, expires_delta=access_token_expires
    )
    response = {"access_token":access_token, "token_type":"bearer"}

    return JSONResponse(status_code=status.HTTP_201_CREATED, content = response)
    # user_dict = user_db.find_one({"username":form_data.username}) # put id here 
    # print(user_dict)
    # user = UserIn(**user_dict)
    # hashed_password = fake_hashed_password(form_data.password)
    # if not hashed_password == user.hashed_password:
    #     raise HTTPException(status_code=400, detail="Incorrect username or password")

    # return {"access_token": user.username, "token_type": "bearer"}



@user.get("/users/me", response_model = User)
async def find_all_users(current_user: User = Depends(get_current_users)):
    # print(conn.test.user.find())  #converts mongo obhject id to python dictionary 
    # print(usersEntity(conn.test.user.find()))
    # x = conn.test.user.find()
    # if x.count() == 0:
    #     raise raise_user_not_found()
    # return usersEntity(conn.test.user.find())
    print(current_user)
    return current_user

@user.get("/users/me/items/")
async def read_own_items(current_user: User = Depends(get_current_users)):
    return [{"item_id": "Foo", "owner": current_user.username}]

# @user.post("/",response_model=UserOut)
# async def create_user(user:User): # Pydantic model defined inside model.user
#     conn.test.user.insert_one(dict(user))    
#     return user
#     # return userEntity(conn.test.user.find()) # response models dont work when i return mongodb query 


@user.put("/{id}")
async def update_users(id,user:User):
    conn.test.user.find_one_and_update({"_id":ObjectId(id)},{"$set":dict(user)})

    return userEntity(conn.test.user.find_one({"_id":ObjectId(id)}))

@user.patch("/{id}")
async def update_selective(id):
    conn.test.user.update_one
    
@user.delete("/{id}")
async def delete_users(id):

    x = conn.test.user.find()
    if x.count() == 0: 
        raise raise_user_not_found()
    conn.test.user.delete_one({"_id":ObjectId(id)})
    return usersEntity(conn.test.user.find())


    

def raise_user_not_found(): # HTTP exception    
    return HTTPException(status_code = 404, detail = "No users detected",headers={"X-header_error":"Nothing to be seen here"})