import asyncio
import time 

# background_task = set()

# async def async_function():
#     print("One")
#     await asyncio.sleep(1)
#     print("Two")



# async def main():
#     task1=  asyncio.create_task(async_function())
#     background_task.add(task1)
#     print(background_task)
#     task2 = asyncio.create_task(as    ync_function())
#     background_task.add(task2)
#     print(background_task)
#     await asyncio.gather(task1,task2)
#     # await asyncio.gather(async_function(),async_function())


# asyncio.run(main())

# to get a factorial
async def factorial(number):
    factorial = 1
    for i in range(2,number + 1):
        factorial *= i
    await asyncio.sleep(1)
    print(f"factorial of {number} is {factorial}")
    return factorial 

async def main():
    task1 = asyncio.create_task(factorial(3))
    task2 = asyncio.create_task(factorial(4))
    task3 = asyncio.create_task(factorial(5))
    L = await asyncio.gather(task1,task2,task3)

    print(L)


asyncio.run(main())