from fastapi import FastAPI
from pymongo import MongoClient
from routes.user import user

app = FastAPI()
app.include_router(user)    