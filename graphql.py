"""
GraphQL lets developers construct requests that pull data from multiple data sources in a single API call.
"""
import strawberry
from strawberry.fastapi import GraphQLRouter
from fastapi import FastAPI


@strawberry.type
class Query:
    @strawberry.field
    def Hello(self) -> str:
        return "Hello world"


schema = strawberry.Schema(Query)
graphql_app = GraphQLRouter(schema)


app = FastAPI()
app.include_router(graphql_app, prefix = "/graphql")