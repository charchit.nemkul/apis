from typing import Union

from fastapi import FastAPI

app = FastAPI()


@app.get("/{item_id}") # path parameters are required to be filled 
async def read_item(item_id:str, q: Union[str, None] = None, short: bool = False):
    item = {"item_id": item_id}
    if q:
        item.update({"q": q})
    if not short:
        item.update(
            {"description": "This is an amazing item that has a long description",
            "whats this ":"something else i added "}
        )
    return item 