from pydantic import BaseModel, Field 
from typing import Union,Optional
from bson import ObjectId


class User(BaseModel):
    _id:ObjectId
    username: str
    email:Union[str,None] = None
    password: Union[str,None] = None
    disabled: Union[bool,None] = None
    # Optional[Union[str,None]] = Field(default=None, title = "lenght must be greater than 4")

    class Config:
        orm_mode = True


class UserIn(User):
    hashed_password: Union[str,None] = None

# x=  {"name":"homer","hashedpassword":"fakenews"}
# print(UserIn(**x)) ##  return what is in the pydantic model 


class Token(BaseModel):
    access_token: str
    token_type: str 


class TokenData(BaseModel):
    username : Union[str,None] = None   

class UserOut(BaseModel): # creating an output model that doesnt include sensitive info 
    name:str
    email:str
    